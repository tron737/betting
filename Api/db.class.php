<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.04.2017
 * Time: 12:57
 */
class db
{
    public function __construct() {
        if($_SERVER['HTTP_HOST'] == 'betting') {
            $this->mysqli = new mysqli("localhost", "root", "", "betting");
        } else {
            $this->mysqli = new mysqli("localhost", "root", "gb7j7y5th", "betting");
        }
        $this->mysqli->set_charset('utf8');
    }

    private function query($sql) {
        $args = func_get_args();

        $sql = array_shift($args);
        $link = $this->mysqli;

        $args = array_map(function ($param) use ($link) {
            return "'".$link->escape_string($param)."'";
        },$args);

        $sql = str_replace(array('%','?'), array('%%','%s'), $sql);

        array_unshift($args, $sql);

        $sql = call_user_func_array('sprintf', $args);


        $this->last = $this->mysqli->query($sql);
        if ($this->last === false) throw new Exception('Database error: '.$this->mysqli->error);

        return $this->last;
    }
    private function assoc() {
        return $this->last->fetch_assoc();
    }
    private function all() {
//        return $this->last->fetch_all();
        $result = array();
        while ($row = $this->last->fetch_assoc()) $result[] = $row;
        return $result;
    }

    public function checkUid() {
        self::query('SELECT `uid`, `access` FROM `allowid` WHERE `uid` = ? AND `access` = 1 LIMIT 1', $_SESSION['user']['uid']);
        return self::assoc();
    }

    public function getHistory() {
        self::query('SELECT * FROM `history` LIMIT 30');
        return self::all();
    }
}