<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.04.2017
 * Time: 14:04
 */
class Authorization
{
    private $clientId = 5972650;
    private $clientSecret = 'QmDOcNXDyhuarbwM6p49';
    private $redirectUri = 'http://betting/?auth/';
    private $url = 'http://oauth.vk.com/authorize';
    private $OAuthUrl = 'https://oauth.vk.com/access_token';
    private $urlUsersGet = 'https://api.vk.com/method/users.get';
    private $code = '';
    private $resultArr;
    public $personalInformation;

    public function __construct() {
        if($this->isLocalhost()) {
            $this->clientId = 5972650;
            $this->clientSecret = 'QmDOcNXDyhuarbwM6p49';
            $this->redirectUri = 'http://betting/?home/';
        } else {
            $this->clientId = 5973454;
            $this->clientSecret = 'w3KB9Em4etQeYnMwljCV';
            $this->redirectUri = 'http://188.120.244.239/?home/';
        }
    }

    private function isLocalhost() {
        return $_SERVER['HTTP_HOST'] == 'betting';
    }

    public function generationAuthUrl() {
        $params = array(
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
            'response_type' => 'code'
        );

        return $this->url . '?' . urldecode(http_build_query($params));
    }

    public function getToken($code) {
        if(!empty($code)) {
            $params = array(
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'code' => $code,
                'redirect_uri' => $this->redirectUri,
            );

            $url = $this->OAuthUrl . '?' . urldecode(http_build_query($params));

            $result = Request::get($url);

            $this->code = $code;

            $this->resultArr = json_decode($result, true);

            if(isset($this->resultArr['access_token'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getPersonalInformation() {
        $params = array(
            'uids' => $this->resultArr['user_id'],
            'fields' => 'uid,photo_100,first_name,last_name',
            'access_token' => $this->resultArr['access_token'],
        );

        $url = $this->urlUsersGet . '?' . urldecode(http_build_query($params));
        $userInfo = json_decode(Request::get($url), true);
        $this->personalInformation = $userInfo['response'][0];
        if(!empty($this->personalInformation)) {
            return true;
        } else {
            return false;
        }
    }
    public function saveToSession() {
        $_SESSION['user'] = $this->personalInformation;
    }
}