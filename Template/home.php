<?
//var_dump($this);
//var_dump($this->model->getHistory());
?>

<? if ($this->mainModel->isAuth) { ?>

    <div>
        Пользователей онлайн <span js-chat-online>0</span>
    </div>

    <div class="chat_window">
        <div class="top_menu">
            <div class="title">Лайв беттинг</div>
        </div>
        <ul class="messages"></ul>
        <div class="bottom_wrapper clearfix">
            <div class="message_input_wrapper"><input class="message_input" placeholder="Ваше сообщение"/></div>
            <div class="send_message">
                <div class="icon"></div>
                <div class="text">Отправить</div>
            </div>
        </div>
    </div>
    <div class="message_template">
        <li class="message">
            <div class="avatar">
                <a href="" target="_blank" title="">
                    <img src="" width="60">
                </a>
            </div>
            <div class="text_wrapper">
                <div class="text"></div>
            </div>
        </li>
    </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script>

    <script type="text/javascript">
        var name = '<?= $_SESSION['user']['first_name'] . ' ' . $_SESSION['user']['last_name'] ?>';
        var uid = '<?= $_SESSION['user']['uid'] ?>';
        var photo = '<?= $_SESSION['user']['photo_100'] ?>';
        var isLocal = '<?= $_SERVER['HTTP_HOST'] == 'betting' ?>';
        var historyMessage = jQuery.parseJSON('<?= $this->model->getHistory() ?>');
    </script>

    <script src="/static/js/chat.js"></script>



<? } else { ?>

    <? if(!empty($this->model->error)) { ?>
        <div class="alert alert-danger">
            <strong><?= $this->model->error ?></strong>
        </div>
    <? } ?>


    <a href="<?= $this->model->displayUrl ?>" class="btn btn-success">Авторизоваться</a>
<? } ?>