<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.02.2017
 * Time: 19:34
 */
class runController extends runModel
{
    public $tpl;
    public $activeJsScript;
    public $activeTpl;
    public $pathUri;
    public $activeFile;
    public $defaultTpl = 'home.php';
    public $mainTemplate = 'mainTemplate.php';
    public $errorPage = '404.php';
    public $TemplateDirectory = '/Template/';
    public $title = array(
        'main' => 'Главная',
    );

    public function runController($path) {
        if(empty($path[0])) {
            $this->title = $this->title['main'];
        } else {
            $this->title = $this->title[$path[0]];
        }
        $this->activeFile = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->mainTemplate;
        $this->activeTpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $path[0] . '.php';

        if(!empty($path[0])
            && file_exists($this->activeTpl)
            && is_readable($this->activeTpl)) {
            $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $path[0] . '.php';
            $this->activeJsScript = $path[0];
        } elseif(empty($path[0])) {
            $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->defaultTpl;
        } else {
            $this->tpl = $_SERVER['DOCUMENT_ROOT'] . $this->TemplateDirectory . $this->errorPage;
        }
        require_once strval($this->activeFile);
    }
}