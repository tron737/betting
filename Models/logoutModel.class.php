<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.04.2017
 * Time: 10:56
 */
class logoutModel
{
    public function __construct() {
        unset($_SESSION['user']);
        header('Location: /');
    }
}