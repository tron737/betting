<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.02.2017
 * Time: 23:11
 */
class runModel
{
    public $modelDir = '/Models/';
    public $nameMainModel = 'mainModel';
    public $defaultModel = 'homeModel'; //name model in main page
    public $mainModel;
    public $model;
    public function runModel($nameModel) {
        //check model file

        require_once $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $this->nameMainModel . '.class.php';
        $this->mainModel = new $this->nameMainModel();

        if(!empty($nameModel)) {
            $nameModel = $nameModel . 'Model';
            $nameModelPath = $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $nameModel . '.class.php';
            $runModel = $nameModel;
        } else {
            $nameModelPath = $_SERVER['DOCUMENT_ROOT'] . $this->modelDir . $this->defaultModel . '.class.php';
            $runModel = $this->defaultModel;
        }

        if (file_exists($nameModelPath) && is_readable($nameModelPath)) {
            require_once $nameModelPath;
            $this->model = new $runModel();
        }
    }
}