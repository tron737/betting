<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.02.2017
 * Time: 23:20
 */
class homeModel
{
    public $displayUrl;
    public $error;
    public $db;

    public function __construct() {
        $auth = new Authorization();
        $this->db = new db();
        $this->displayUrl = $auth->generationAuthUrl();
        if(isset($_REQUEST['code'])) {
            $auth->getToken($_REQUEST['code']);
            $getAuth = $auth->getPersonalInformation();
            if($getAuth) {
                $auth->saveToSession();
                $access = $this->db->checkUid();
                if($access !== null) {
                    //даем доступ
                    header('Location: /');
                } else {
                    $this->error = 'Извините, но мы не нашли Вас в нашей базе.';
                }
            }
        }
    }

    public function getHistory() {
        $arr = $this->db->getHistory();
        return json_encode($arr);
    }
}