<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.02.2017
 * Time: 23:32
 */
class mainModel
{
    public $test = 'erg';
    public $isAuth = false;

    public function __construct() {
        session_start();
        $this->isAuth = self::isAuth();
    }

    public function isAuth() {
        return isset($_SESSION['user']) && !empty($_SESSION['user']);
    }
}