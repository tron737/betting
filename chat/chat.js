var port = 3000;

var options = {};

var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server, options);
var mysql = require('mysql');

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'gb7j7y5th',
    database : 'betting',
});

server.listen(port);

console.log('SERVER START');

io.sockets.on('connection', function (client) {
	console.log('CLIENT CONNECTION');
	// console.log(io.sockets.server.eio.clientsCount);
	console.log(io.sockets.adapter.rooms);
    client.emit('info', {online: io.sockets.server.eio.clientsCount});
	client.broadcast.emit('info', {online: io.sockets.server.eio.clientsCount});
	client.on('message', function (message) {
		try {
			console.log('MESSAGE');
			console.log(message);
			client.broadcast.send(message);
            var query = connection.query('INSERT INTO `history`(`message`, `uid`, `name`, `photo`) VALUES ("'+ message.message +'", "'+ message.uid +'", "'+ message.name +'", "'+ message.photo +'")', function(err, result) {
                console.log(err);
            });
		} catch(e) {
			console.log(e);
			client.disconnect();
		}
	});
});

io.sockets.on('disconnect', function (client) {
	console.log('CLIENT DISCONNECT');
    client.emit('info', {online: io.sockets.server.eio.clientsCount});
    connection.end();
});