<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.02.2017
 * Time: 18:11
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/Models/runModel.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Controllers/runController.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Application/Application.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/Authorization.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/db.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/Api/Request.class.php';

new Application(substr($_SERVER['REQUEST_URI'], 2));