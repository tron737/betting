/**
 * Created by admin on 08.04.2017.
 */
$(document).ready(function() {

    // if(isLocal != '') {
    //     var socket = io.connect('http://localhost:3000');
    // } else {
        var socket = io.connect('http://188.120.244.239:3000');
    // }
    var getMessageText, message_side, sendMessage;
    var message_txt = $('input.message_input');
    var Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side, this.avatar = arg.avatar, this.uid = arg.uid, this.name = arg.name;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $message.find('.avatar a').attr('href', 'https://vk.com/id' + _this.uid);
                $message.find('.avatar a').attr('title', _this.name);
                $message.find('.avatar img').attr('src', _this.avatar);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };

    getMessageText = function () {
        var $message_input;
        $message_input = $('.message_input');
        return $message_input.val();
    };
    sendMessage = function (text, message_side, name, uid, photo) {
        var $messages, message;
        /*if (text.trim() === '') {
            return;
        }*/
        $('.message_input').val('');
        $messages = $('.messages');
        // message_side = message_side === 'left' ? 'right' : 'left';
        message = new Message({
            text: text,
            message_side: message_side,
            name: name,
            avatar: photo,
            uid: uid,
        });
        message.draw();
        return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
    };
    $('.send_message').click(function (e) {
        socket.emit("message", {message: getMessageText(), name: name, uid: uid, photo: photo});
        $('.message_input').focus();
        return sendMessage(getMessageText(), 'right', name, uid, photo);
    });
    $('.message_input').keyup(function (e) {
        if (e.which === 13) {
            socket.emit("message", {message: getMessageText(), name: name, uid: uid, photo: photo});
            $('.message_input').focus();
            return sendMessage(getMessageText(), 'right', name, uid, photo);
        }
    });

    $.each(historyMessage, function(index, elem) {
        //console.log(elem);
        //text, message_side, name, uid, photo
        if(uid == elem.uid) {
            side = 'right';
        } else {
            side = 'left';
        }
        sendMessage(elem.message, side, elem.name, elem.uid, elem.photo);
    });

    // sendMessage('Hello Philip! :)');
    // setTimeout(function () {
    //     return sendMessage('Hi Sandy! How are you?');
    // }, 1000);
    // return setTimeout(function () {
    //     return sendMessage('I\'m fine, thank you!');
    // }, 2000);

    // socket.emit("message", {message: text, name: name});

    socket.on('info', function(data) {
        $('[js-chat-online]').text(data.online);
        // console.log(data);
    });

    socket.on('connecting', function () {
        // msg_system('Соединение...');
    });

    socket.on('connect', function () {
        // msg_system('Соединение установлено!');
    });

    socket.on('message', function (data) {
        // sendMessage('Hi Sandy! How are you?');
        // msg(data.name, data.message);
        sendMessage(data.message, 'left', data.name, data.uid, data.photo);
        message_txt.focus();
    });

    socket.on('disconnected', function() {
    });

    /*
    * <img src="https://pp.userapi.com/c623123/v623123172/3254f/NbUAvPn_cCs.jpg" width="60">
    * */


    // var messages = $("#messages");
    // var message_txt = $("#message_text")
    // $('.chat .nick').text(name);
    //
    // function msg(nick, message) {
    //     var m = '<div class="msg">' +
    //         '<span class="user">' + safe(nick) + ':</span> '
    //         + safe(message) +
    //         '</div>';
    //     messages
    //         .append(m)
    //         .scrollTop(messages[0].scrollHeight);
    // }
    //
    // function msg_system(message) {
    //     var m = '<div class="msg system">' + safe(message) + '</div>';
    //     messages
    //         .append(m)
    //         .scrollTop(messages[0].scrollHeight);
    // }
    //
    // socket.on('connecting', function () {
    //     msg_system('Соединение...');
    // });
    //
    // socket.on('connect', function () {
    //     msg_system('Соединение установлено!');
    // });
    //
    // socket.on('message', function (data) {
    //     msg(data.name, data.message);
    //     message_txt.focus();
    // });
    //
    // $("#message_btn").click(function () {
    //     var text = $("#message_text").val();
    //     if (text.length <= 0)
    //         return;
    //     message_txt.val("");
    //     socket.emit("message", {message: text, name: name});
    // });
    //
    // function safe(str) {
    //     return str.replace(/&/g, '&amp;')
    //         .replace(/</g, '&lt;')
    //         .replace(/>/g, '&gt;');
    // }

});