$.validator.addMethod("requiredemail", function(value, element) {
    pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    return pattern.test(value);
}, "Не валид!");

$(document).ready(function() {

    $('.auth-form').validate({
        rules: {
            email: {
                requiredemail: true
            },
            password: {
                required: true
            }
        },
        submitHandler: function() {
            $.post("/?index", {data: $(".auth-form").serialize()}, function(data) {
                console.log(data);
                $('.loader').show();
                if(data.status == 'ok') {
                    $(".auth-form").hide();
                    $(location).attr('href', '/');
                } else {
                    $(".auth-form").show();
                    $('.loader').hide();
                }
            }, "json");
        }
    });

    $('.reg-form').validate({
        email: {
            requiredemail: true,
        },
        password: {
            required: true
        },
        confirm: {
            equalTo: password
        },
        submitHandler: function() {
            $.ajax({
                type: "POST",
                url: "/?registration",
                data: {'data': $(".reg-form").serialize()},
                dataType: "JSON",
                success: function(data) {
                    $('.loader').show();
                    if(data.status == 'ok') {
                        $(".reg-block").hide();
                        $(location).attr('href', '/');
                    } else {
                        $(".reg-block").show();
                        $('.loader').hide();
                    }
                }
            });
        }
    });

    $(".add-account").submit(function () {
        formValid = true;
        // $('input').each(function() {
        //     if (this.checkValidity() && formValid) {
        //         $(this).addClass('has-success').removeClass('has-error');
        //     } else {
        //         $(this).addClass('has-error').removeClass('has-success');
        //         formValid = false;
        //     }
        // });
        // console.log(formValid);
        if (formValid) {
            $.post("/?checkoraddacc", {data: $(this).serialize()}, function (data) {
                console.log(data);
                $('.loader').show();
                if (data.status == "ok") {
                    $(".status").text("");
                    $(location).attr('href', '/?accounts');
                } else {
                    $(".loader").hide();
                    $(".status").text("Не верный логин или пароль / не рабочая прокся / аккаунт уже добавлен");
                }
            }, "json");
        }

        return false;
    });

    $(".btn-del").on('click', function() {
        console.log("del");
        console.log($(this).data('id'));
        $.post("/?deleteacc", {data: $(this).data('id')}, function (data) {
            console.log(data);
            if(data.status == "ok") {
                $(location).attr('href', '/?accounts');
            }
        }, "json");
    });

    $(".btn-edit").on('click', function() {
        console.log("edit");
        console.log($(this).data('id'));
        input = "form.edit-account";

        $.post("/?getEditData", {data:$(this).data('id')}, function(data) {
            console.log(data);
            if(data.status == "ok") {
                $(input+" #login").val(data.login);
                $(input+" #password").val(data.password);
                $(input+" #proxy").val(data.ip_proxy);
                $(input+" #proxyport").val(data.ip_port);
                $(input+" #auth-proxy").val(data.auth_proxy);
                $(input+" #type-proxy").val(data.type_proxy);
                $(input+" #likesday").val(data.likes_day);
                $(input+" #id").val(data.id);
            }
            console.log(data);
        }, "json");
    });

    $(".btn-close-edit").on('click', function() {
        input = "form.edit-account";
        $(input+" #login").val("");
        $(input+" #password").val("");
        $(input+" #proxy").val("");
        $(input+" #proxyport").val("");
        $(input+" #auth-proxy").val("");
        $(input+" #type-proxy").val("");
        $(input+" #likesday").val("");
        $(input+" #id").val("");
    });

    $(".edit-account").submit(function() {
        $.post("/?saveEditData", {data: $(this).serialize()}, function (data) {
            console.log(data);
            if(data.status == "ok") {
                $(".status").text("");
                $(location).attr('href', '/?accounts');
            } else {
                $(".status").text("Перепроверьте данные");
            }
        }, "json");
        return false;
    });
});